library(shiny)
library(timevis)
library(readxl)
library(tidyr)
library(dplyr)
library(lubridate)
library(shinythemes)
library(glue)
library(fs)
library(shinyalert)
library(shinyWidgets)
library(stringr)
library(shinyBS)
library(rhandsontable)
library(DBI)
library(dbplyr)
library(RPostgreSQL)
library(aws.s3)
library(writexl)

#Get Data and label for the sake of Timevis
conRS <- dbConnect(
  RPostgreSQL::PostgreSQL(),
  dbname = 'i360reporting',
  host = 'i360-grassroots-prod.clxvtm8tehu9.us-east-1.redshift.amazonaws.com',
  port = 5439,
  user = "dataops",
  password = "7^3HE4N3Uysds$aaNx??"
)
#sql <- config::get('datasvcs', file = "db.yml")
onStop(function() {
  dbDisconnect(conRS)
})

awsKey <- "AKIAYI57YHXH6ATMGOGM"
awsSecretAccessKey <- "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"
endpoint <- "s3.amazonaws.com"
i360Bucket <- "i360-prod-timelineapp"


stringGen <- function(n = 5) {
  a <- do.call(paste0, replicate(8, sample(LETTERS, n, TRUE), FALSE))
  paste0(a, sprintf("%04d", sample(9999, n, TRUE)), sample(LETTERS, n, TRUE))
}

datFiles <- get_bucket_df(
  bucket = i360Bucket
  , key = awsKey
  , base_url = endpoint
  , secret = awsSecretAccessKey
) %>% select(Key)

uploadFunction <- function(a){
  fileU <- a
  fileCode<- paste(Sys.Date(),'_',fileU$name,sep='')
  put_object(
    file = fileU$datapath
    , object = paste('Timeline PDF/',fileCode,sep='')
    , bucket = i360Bucket
    , key = awsKey
    , base_url = endpoint
    , secret = awsSecretAccessKey
  )}


# Define UI
ui <- fluidPage(
  tags$style('#entry * { word-wrap: break-word};
          #uppies { margin-bottom: 0 !important}'),
  # style = "padding-top: 250px;",
  #theme = shinytheme("lumen"),
  tags$head(
    tags$link(href = "style5.css", rel = "stylesheet"),
    tags$script(src = "vis-timeline-graph2d.min.js"),
    HTML('<script src="ResizeSensor.js"></script>
<script src="ElementQueries.js"></script>')
    
  ),
  wellPanel(titlePanel("Timeline Demonstration")),
  #Timeline
  fluidRow(
    HTML(
      '<button data-toggle="collapse" data-target="#demo">Filters</button>'
    ),
    tags$div(
      id = 'demo',
      class = "collapse in",
      wellPanel(
        fluidRow(
          column(3, uiOutput("timeline_dates")),
          column(
            3,
            radioGroupButtons(
              label = 'Group by Category',
              inputId = "GroupSwitch",
              choices = c("Group by Category", "Ungroup"),
              status = "primary"
            )
          ),
          column(
            3,
            uiOutput('categoryUI')
            
          )
          ,
          column(
            3,
            uiOutput('subcategoryUI')
          ),
        ),
        fluidRow(column(
          12,
          div(
            id = "tl_button",
            actionButton("fit", "Show All Items"),
            actionButton("default", "Reset Zoom"),
            downloadButton("dl", "Download All Data")
            #,actionButton("refresh", "Refresh Data")
          )
        ))
      )
    )
  ),
  
  tabsetPanel(
    id = 'panels',
    tabPanel("Timeline",
             value = 1,
             wellPanel(timevisOutput("TimelineVis"))),
    # tabPanel("All Data (Show Only)",
    #          value = 2,
    #          # downloadButton("dl", "Download"),
    #          wellPanel(rHandsontableOutput("TableEdits"))),
    tabPanel(
      'Data Entry',
      value = 3,
      h3("DATES MUST BE IN MONTH/DAY/YEAR"),
      h3("ENTRIES WITHOUT A CATEGORY WILL NOT BE UPLOADED"),
      actionButton("saveBtn", "Save"),
      # fluidRow(
      #   HTML(
      #     '<button data-toggle="collapse" data-target="#attachments">File Attach</button>'
      #   ),
      #   tags$div(
      #     id = 'attachments',
      #     class = "collapse",
      #     wellPanel(fileInput("fileUploadData", NULL, accept = c(".pdf",".png",".jpg"))))
      #       
      # ),
      sidebarLayout(
        sidebarPanel = tags$div(id = 'uppies',
                                sidebarPanel(
                                div(class = 'uploadButtons',
                                      HTML("<table class='toMatch'><tbody>"),
                                      lapply(1:20,function(i){
                                        tags$tr(tags$td(div(fileInput(paste0("fileUploadData_",i), NULL, accept = c(".pdf","png","jpg"), buttonLabel = 'Upload Attachment'))))
                                      })
                                    ,HTML("</tbody></table>")
                                      ))
        ),
        mainPanel =  tags$div(id = 'entry',
                              mainPanel(
                                wellPanel(rHandsontableOutput("hot"))),
                                      HTML("<script>
                                      new ResizeSensor($('#entry .well'), function() {
                 $('.toMatch tbody tr').each(function(i) {
                 var tableOneRow = this;
                 var tableTwoRow = $('.htCore tbody tr').get(i);
                 $(this).height($(tableTwoRow).height());
                 });
                                      });
                 </script>")),
        position = c("right")),
    )
    # ,
    # tabPanel(
    #   'PDF Upload',
    #   value = 4,
    #   fileInput("fileUpload", NULL, accept = c(".pdf"))
    # )
  ),
  
)
options(shiny.sanitize.errors = TRUE)

# Server
server <- function(input, output, session) {
  
  Sample_Data_timeline <-
    dbGetQuery(conRS, "SELECT * from analytics.kms_timelinetest2")
  # Sample_Data_timeline$date <- mdy(Sample_Data_timeline$date)
  
  SDT <- Sample_Data_timeline[1:10,]
  
  # SDT$date <- seq(from = Sys.Date(), by = "days", length.out = 10)
  SDT$date[2:10] <- NA
  #rhandsontable(SDT)
  SDT[1:20, 2:length(colnames(SDT))] <- NA
  
  # SDT[1,1] <- 'Test'
  # SDT[2,1] <- '02-02-2011'
  # SDT <- SDT %>% filter(!is.na(date))
  
  SDTBlank <- data.frame(date = seq(from = Sys.Date(), by = "days", length.out = 1), SDT)
  SDTBlank$date[1:20] <- NA
  
  datTabs <-
    rhandsontable(Sample_Data_timeline, 
                  search = TRUE,
                  height = 600,
                  useTypes = FALSE,
                  #rowHeaderWidth = 100
    ) %>%
    hot_cols(columnSorting = TRUE)  %>%
    # hot_context_menu(
    #   customOpts = list(
    #     search = list(name = "Search",
    #                   callback = htmlwidgets::JS(
    #                     "function (key, options) {
    #                        var srch = prompt('Search criteria');
    # 
    #                        this.search.query(srch);
    #                        this.render();
    #                      }")))) %>% 
    hot_col(2:length(Sample_Data_timeline), colWidths = 180)
  
  VisDat <- unique(Sample_Data_timeline)
  colnames(VisDat)[1] <- 'start'
  colnames(VisDat)[2] <- 'content'
  colnames(VisDat)[3] <- 'title'
  VisDat$start <- mdy(VisDat$start)
  VisDat <- VisDat %>% filter(is.na(start) == FALSE)
  VisDat$CapDate <- format(VisDat$start, '%B %d, %Y')
  #For Timevis Grouping
  VisDat <-
    dplyr::rename(
      VisDat,
      Category = category,
      `Sub-Category` = `sub-category`,
      Link = link,
      # Document = document,
      Citation = citation
    )
  categoryField <- unique(VisDat$Category)
  subcategoryField <- unique(c('',filter(VisDat,!is.na(`Sub-Category`))$`Sub-Category`))
  VisDat$group <- VisDat$Category
  VisDat$id <- seq.int(nrow(VisDat))
  VisDat <- filter(VisDat, is.na(group) == FALSE)
  grouping <- unique(data.frame(
    id = c(VisDat$group),
    #content = c(''),
    content = c(VisDat$group),
    className = c('')
  ))
  grouping2 <- unique(data.frame(
    id = 'Timeline',
    content = '',
    className = 'Timeline'
  ))
  grouping3 <- unique(data.frame(
    id = c(VisDat$group),
    #content = c(''),
    content = '',
    className = c('')
  ))
  grouping$className <-
    c(paste('A', 1:length(unique(VisDat$group)), sep = ''))
  VisDat <-
    left_join(
      VisDat,
      select(grouping, Category = content, className),
      by = ('Category'),
      all.x = TRUE
    )
  
  
  VisDat <-
    VisDat %>% mutate(
      content = glue(
        "<div class = \"TL\"><strong>{VisDat$CapDate}</strong><div>{content}</div></div>"
      )
    )
  #VisDat <- VisDat %>% mutate(content = as.character(div(class = "TL",strong(lubridate::year(VisDat$start)),div(content))))
  VisDat <- VisDat %>% mutate(SubCat = case_when(is.na(`Sub-Category`) ~ Category,
                                                 TRUE ~ `Sub-Category`))
  #VisDat$end <- as.Date(VisDat$start) + 3650
  VisDat$type <- 'box'
  VisDat <- VisDat %>% mutate(
    style = case_when(
      className %in% c('A1') ~ 'color: #1BC5BD; background-color: #C9F7F5;',
      className %in% c('A2') ~ 'color: #FFA800; background-color: #FFF4DE;',
      className %in% c('A3') ~ 'color: #8950FC; background-color: #EEE5FF;',
      className %in% c('A4') ~ 'color: #3699FF; background-color: #E1F0FF;',
      className %in% c('A5') ~ 'color: #5B4931; background-color: #D4B483;',
      className %in% c('A6') ~ 'color: #49ad70; background-color: #a5d4b8;',
      className %in% c('A7') ~ 'color: #4285f5; background-color: #a2c2f7;',
      className %in% c('A8') ~ 'color: #ff6901; background-color: #fdb38f;',
      className %in% c('A9') ~ 'color: #891b70; background-color: #c28fb7;',
      className %in% c('A10') ~ 'color: #232066; background-color: #9090b1;',
      className %in% c('A11') ~ 'color: #6D6D6D; background-color: #c4cfd7;',
      className %in% c('A12') ~ 'color: #ED2089; background-color: #FDEBF3;',
      className %in% c('A13') ~ 'color: #ADA8B6; background-color: #4C3B4D;',
      className %in% c('A14') ~ 'color: #C6E5D9; background-color: #61C9A8;',
      className %in% c('A15') ~ 'color: #FCD0E1; background-color: #A53860;',
      className %in% c('A16') ~ 'color: #4C3B4D; background-color: #ADA8B6;',
      className %in% c('A17') ~ 'color: #F99D41; background-color: #FFEEDB;',
      className %in% c('A18') ~ 'color: #B2F7C4; background-color: #295135;',
      className %in% c('A19') ~ 'color: #E5F7D2; background-color: #5A6650;',
      className %in% c('A20') ~ 'color: #295135; background-color: #9FCC2E;',
      className %in% c('A21') ~ 'color: #0A440A; background-color: #9EBC9E;',
      className %in% c('A22') ~ 'color: #FFD2D5; background-color: #A26769;',
      className %in% c('A23') ~ 'color: #FC5927; background-color: #FB9874;',
      className %in% c('A24') ~ 'color: #5B4931; background-color: #D4B483;',
      TRUE ~ 'color: #5B4931; background-color: #D4B483;'
    )
  )
  
  
  
  #Doing the actual filtering
  
  output$categoryUI <- renderUI({pickerInput(
    label = 'Category',
    inputId = 'Category',
    choices = c('All', unique(VisDat$Category)),
    selected = 'All',
    multiple = TRUE,
    options = list(`actions-box` = TRUE))
  })
  
  output$subcategoryUI <- renderUI({pickerInput(
    label = 'Sub Category',
    inputId = 'Source',
    choices = c('All', ''),
    multiple = TRUE,
    selected = 'All',
    options = list(`actions-box` = TRUE)
  )
  })
  
  
  VisDatUse <- reactive({
    req(VisDat, cancelOutput = TRUE)
    if ('All' %in% input$Category) {
      VisDatUse <- VisDat
    }
    else {
      VisDatUse <- subset(VisDat, Category %in% c(input$Category))
    }
    if ('All' %in% input$Source | length(input$Source) == 0) {
      VisDatUse <- VisDatUse
    }
    else {
      VisDatUse <- subset(VisDatUse, SubCat %in% c(input$Source))
    } %>% arrange(start)
    return(VisDatUse)
  })
  
  observe({
    if ('All' %in% input$Category) {
      updatePickerInput(session, 'Source', choices = c('All', unique(VisDat$SubCat)))
    }
    else {
      updatePickerInput(session, 'Source', choices = c('All', unique(
        subset(VisDat, Category %in% input$Category)$SubCat
      )))
    }
  })
  
  #Time filter
  output$timeline_dates <- renderUI({
    min_date <- VisDatUse() %>%
      select(start) %>%
      summarize(min(start)) %>%
      pull()
    
    max_date <- VisDatUse() %>%
      select(start) %>%
      summarize(max(start)) %>%
      pull()
    
    dateRangeInput(
      inputId = "timeline_dates_range",
      label = "Select Date Range:",
      start = ymd(min_date),
      end = ymd(max_date),
      min = ymd(min_date),
      format = "mm-dd-yyyy",
      startview = "years",
      width = "100%"
    )
  })
  VisDatUse2 <- reactive({
    VisDatUse2 <- subset(VisDatUse(), Category == 'NothingToSeeHere')
    return(VisDatUse2)
  })
  
  
  min_date <- reactive({
    MD <- VisDatUse()$start[1]
    return(MD)
  })
  
  max_date <- reactive({
    MD <- VisDatUse() %>% arrange(desc(start))
    return(MD)[1]
  })
  
  groupingFilter <- reactive({
    groupF <- grouping %>% filter(id %in% VisDatUse()$Category)
    return(groupF)
  })
  
  groupingFilter2 <- reactive({
    groupF <- grouping2
    return(groupF)
  })
  
  #Timevis output
  output$TimelineVis <-
    renderTimevis({
      req(VisDatUse, cancelOutput = TRUE)
      config <- list(
        zoomKey = "ctrlKey",
        maxHeight = '800px',
        orientation = list(axis = 'top', item = 'top'),
        start = input$timeline_dates_range[1],
        end = input$timeline_dates_range[2],
        align = 'left',
        verticalScroll = TRUE
      )
      if (input$GroupSwitch == 'Group by Category') {
        timevis(
          select(VisDatUse(),-type) %>% mutate(type = 'point'),
          groups = groupingFilter(),
          options = config
        )
      }
      else{
        timevis(select(VisDatUse(), -group),
                options = config)
      }
    })
  
  
  
  #Extracting the ids from the selections on the timeline and passing them through to get the PDF code
  values <- reactiveValues(TimelineVis_selected = NULL)
  
  observe({
    values$TimelineVis_selected <- input$TimelineVis_selected
  })
  
  
  
  # TableCheck <-
  #   reactive({
  #     if (!is.null(values$TimelineVis_selected)) {
  #       input$TimelineVis_data %>%
  #         filter(id == values$TimelineVis_selected) %>%
  #         select(Document)
  #       
  #     }
  #     
  #   })
  
  TableCheckDL <-
    reactive({
      if (!is.null(values$TimelineVis_selected)) {
        input$TimelineVis_data %>%
          filter(id == values$TimelineVis_selected) %>%
          select(documents3)
        
      }
    })
  
  #Outputting the proper PDF based on its PDF code, code is the number after TestPDF on the filename
  # output$iframeUI <-
  #   renderUI({
  #     req(TableCheck(), cancelOutput = TRUE)
  #     if (!is.na(TableCheck())) {
  #       tags$iframe(style = "height:600px; width:100%; scrolling=yes",
  #                   src = glue("resources/TestPDF",
  #                              unlist(TableCheck()[1]),
  #                              ".pdf"))
  #       
  #     } else {
  #       #h3("No Files Available")
  #       h3(paste(unlist(TableCheckDL()[1])))
  #     }
  #     
  #   })
  # 
  # observeEvent(input$TimelineVisDot_selected,{
  #   output$pdfDownload <- downloadHandler({
  #     # pdfDL <- input$TimelineVis_data %>%
  #     #   filter(id == input$TimelineVisDot_selected) %>%
  #     #   select(documents3)
  #     # pdfDL <- TableCheck()[1]
  #     pdfDL <- 'DYVTYASF1030D'
  #     filename = paste0(pdfDL,'.pdf')
  #     content = function(file) {
  #       tmp <- tempfile(fileext = '.pdf')
  #       
  #       fileName <- paste0(pdfDL,'.pdf')
  #       
  #       get_object(
  #         paste0("Timeline PDF/", fileName, sep='')
  #         , file = tmp
  #         , bucket = i360Bucket
  #         , key = awsKey
  #         , base_url = endpoint
  #         , secret = awsSecretAccessKey)
  #       
  #       file.copy(tmp, file)}
  #     
  #   })
  # })
  
  # test_result_path <- file_temp("test_result_pdf", tmp_dir = "www", ext = ".pdf")
  # test_result_path <- file_temp("test_result_pdf", tmp_dir = "www")
  #output[[paste0("showAttach_",rv$Counter)]]
  observeEvent(input$Attach,{
    output$showAttach <- renderUI({
      pdfDL <- TableCheckDL()$documents3[1]
      if(isTruthy(pdfDL)){
        if(str_sub(pdfDL, start = -3) == 'pdf'){
          test_result_path <- file_temp("test_result_pdf", tmp_dir = "www", ext = ".pdf")
          tmp <- tempfile(fileext = '.pdf')
        } else if(str_sub(pdfDL, start = -3) == 'png'){
          test_result_path <- file_temp("test_result_pdf", tmp_dir = "www", ext = ".png")
          tmp <- tempfile(fileext = '.png')
        }else if(str_sub(pdfDL, start = -3) == 'jpg'){
          test_result_path <- file_temp("test_result_pdf", tmp_dir = "www", ext = ".jpg")
          tmp <- tempfile(fileext = '.jpg')
          
        }
        # fileName <- paste0(pdfDL,'.pdf', sep='')
        fileName <- pdfDL
        
        # tmp <- tempfile(fileext = '.pdf')
        # tmp <- tempfile()
        
        
        get_object(paste0("Timeline PDF/", fileName, sep='')
                   , file = tmp
                   , bucket = i360Bucket
                   , key = awsKey
                   , base_url = endpoint
                   , secret = awsSecretAccessKey) %>%
          writeBin(test_result_path)
        tags$iframe(style = "height:1400px; width:100%", src = str_sub(test_result_path, 5))
      } else {h3("No Files Found")}
    })
  })
  
  
  
  
  output$dl <- downloadHandler(
    filename = function() { "Full Timeline Data.xlsx"},
    content = function(file) {write_xlsx(Sample_Data_timeline, path = file)}
  )
  
  observeEvent(input$TimelineVis_selected, {
    req(values$TimelineVis_selected)
    Link <- input$TimelineVis_data %>%
      filter(id == values$TimelineVis_selected) %>%
      select(Link)
    title <-
      input$TimelineVis_data[input$TimelineVis_data$id == values$TimelineVis_selected, "title"]
    LinkText <- 'Link'
    if (is.na(Link)) {
      Link <- LinkText <- ''
    }
    showModal(modalDialog(
      title = "Details",
      title,
      tags$div(
        tags$a(
          href = Link,
          target = "_blank",
          rel = "noreferrer noopener",
          LinkText
        )
      )
      ,
      actionButton("Attach", "Attachments")
      # ,downloadButton("pdfDownload", label = "Download")
    ))
    
  })
  
  
  
  
  observeEvent(input$TimelineVisDot_selected, {
    Link <- input$TimelineVis_data %>%
      filter(id == input$TimelineVisDot_selected) %>%
      select(Link)
    title <-
      input$TimelineVis_data[input$TimelineVis_data$id == input$TimelineVisDot_selected, "title"]
    LinkText <- 'Link'
    if (is.na(Link)) {
      Link <- LinkText <- ''
    }
    
    showModal(modalDialog(
      title = "Details",
      title,
      tags$div(
        tags$a(
          href = Link,
          target = "_blank",
          rel = "noreferrer noopener",
          LinkText
        )
      )
      ,
      actionButton("Attach", "Attachments")
      #,downloadButton("pdfDownload", label = "Download")
    ))
    
  })
  observeEvent(input$Attach, {
    req(values$TimelineVis_selected)
    showModal(modalDialog(
      title = "Details",
      paste(
        input$TimelineVis_data %>%
          filter(id == values$TimelineVis_selected) %>%
          select(content)
      )
    ))
  })
  
  
  
  observeEvent(input$Attach, {
    showModal(modalDialog(
      title = "Important message",
      uiOutput("showAttach"),
      # uiOutput("iframeUI"),
      easyClose = TRUE,
      size = 'l'
    ))
  })
  
  observeEvent(input$fit, {
    fitWindow("TimelineVis")
  })
  observeEvent(input$fit, {
    fitWindow("TimelineVis2")
  })
  
  observeEvent(input$default, {
    setWindow("TimelineVis",
              input$timeline_dates_range[1],
              input$timeline_dates_range[2])
  })
  observeEvent(input$default, {
    setWindow("TimelineVis2",
              input$timeline_dates_range[1],
              input$timeline_dates_range[2])
  })
  
  output$TableEdits <- renderRHandsontable({
    datTabs
  })
  
  output$hot <- renderRHandsontable({
    rhandsontable(select(SDT, -documents3)
                  # ,colWidths = 100
                  ,rowHeights= 75
                  ,manualColumnResize = TRUE
                  ,manualRowResize = TRUE
                  ,stretchH = "all"
                  #, rowHeaderWidth = 200
    ) %>% hot_col(2:length(select(SDT, -documents3))
                  , colWidths = 180
                  ) %>%
      hot_col(col = "category", type = "dropdown", source = categoryField) %>%
      hot_validate_character(cols = "category", choices = c('',categoryField)) %>%
      hot_col(col = "sub-category", type = "dropdown", source = subcategoryField) %>%
      hot_validate_character(cols = "sub-category", choices = c('',NULL,subcategoryField))
  })
  
  # observeEvent(input$saveBtn, {
  #       showModal(modalDialog(
  #     title = "WARNING",
  #     paste("ENSURE YOUR DATE IS PROPERLY FORMATTED"),
  #    actionButton("saveBtn2", "UPLOAD")))
  #
  # })
  
  observe({
    lapply(1:20,function(i){
      fileID <- paste0("fileUploadData_",i)
      value <- isolate(input[[fileID]]$name)
      ds3 <- paste(Sys.Date(),'_',value,sep='')
      observeEvent(input[[fileID]],uploadFunction(input[[fileID]]))
    })
  }) 
  
  # observeEvent(input$saveBtn,{
  #   fileList <- lapply(1:20,function(i){
  #     fileID <- paste0("fileUploadData_",i)
  #     print(fileID)
  #     value <- input[[fileID]]$name
  #     print(value)
  #     ds3 <- paste(Sys.Date(),'_',value,sep='')
  #     # observeEvent(input[[fileID]],uploadFunction(input[[fileID]]))
  #     if(is.null(value)){data.frame(NULL)}else{data.frame(uploadNo = i, documents3 = ds3,value=value)}
  #     
  #   })
  #   printList <- do.call(rbind,fileList)
  #   print(printList)
  #   return(printList)
  #   
  # })  
  
  uploadList <- reactive({#eventReactive(input$saveBtn,{

    fileList <- lapply(1:20,function(i){
      fileID <- paste0("fileUploadData_",i)
      print(fileID)
      value <- input[[fileID]]$name
      print(value)
      ds3 <- paste(Sys.Date(),'_',value,sep='')
      observeEvent(input[[fileID]],uploadFunction(input[[fileID]]))
      if(is.null(value)){data.frame(uploadNo = NULL, documents3 = NULL,value = NULL)}else{data.frame(uploadNo = i, documents3 = ds3,value=value)}
      
    })
    return(do.call(rbind,fileList))
    
  })
  uploadNo <- 0
  documents3 <- 'a'
  value <- 'b'
  fileListBase <- data.frame(uploadNo, documents3, value)
  
  observeEvent(input$fileUploadData_1,{
    uploadFunction(input$fileUploadData_1)
  })
  
  fileCode1 <- reactive({
    fileU1 <- input$fileUploadData_1
    fileCode1<- paste(Sys.Date(),'_',fileU1$name,sep='')
    print(fileCode1)
    return(fileCode1)
  })
  
  
  observeEvent(input$saveBtn, {
    # remove button and isolate to update file automatically
    # after each table change
    
    hot = isolate(input$hot)
    if (!is.null(hot)) {
      # write.table(hot_to_r(input$hot), fname)
      TbF <- hot_to_r(input$hot)
      # print(fname)
      TbFilter <-
        as.data.frame(TbF) %>% filter(!is.na(date) &
                                        date != '' & category != '') %>% unique()
      
      TbFilter <-
        as.data.frame(lapply(TbFilter, function(x)
          gsub("\"", "", x)))
      TbFilter <-
        as.data.frame(lapply(TbFilter, function(x)
          gsub("\'", "", x)))
      TbFilter$uploadNo <- seq.int(nrow(TbFilter))
      print(as.data.frame(uploadList()))
      print("Check")
      print(TbFilter)
      if (all(is.na(uploadList())) == TRUE) {
        TbFilterJoin <- left_join(TbFilter, as.data.frame(fileListBase), by = 'uploadNo')
      } else{
        TbFilterJoin <-
          left_join(TbFilter, as.data.frame(uploadList()), by = 'uploadNo')
      }
      TbFilterUpload <- select(TbFilterJoin, -uploadNo, -value)
      # TbFilter$documents3 <= fileCode1()
      
      
      
      if (sum(ifelse(is.na(mdy(
        TbFilter$date
      )), 1, 0)) > 0) {
        showModal(modalDialog(title = "WARNING",
                              paste(
                                "ENSURE YOUR DATE IS PROPERLY FORMATTED"
                              )))
      } else{
        fileNameGen <- stringGen()
        # if(input$fileAttach == TRUE){ 
        #   print('Check')
        #   
        # } else {print('NoCheck')}
        tabname <- 'analytics.kms_timelinetest2'
        insertQuery <- TbFilterUpload %>%
          mutate_all(as.character) %>%
          mutate_all( ~ gsub("'", "", .)) %>%
          mutate_all( ~ coalesce(., '@@@@@')) %>%
          unite(values, sep = "','") %>%
          mutate(values = paste0("('", values, "')")) %>%
          pull(values) %>%
          paste(., collapse = ",") %>%
          paste("INSERT INTO", tabname, "VALUES", .) %>%
          gsub("'@@@@@'", 'null', .)
        print("Starting Query")
        dbExecute(conRS, insertQuery)
        print(insertQuery)
        showModal(modalDialog(
          title = "SUCCESS",
          paste(
            "DATA ENTERED. PLEASE REFRESH THE APPLICATION. UPDATES MAY TAKE A MINUTE"
          )
        ))
        print('Success')
        output$hot <- renderRHandsontable({
          rhandsontable(select(SDT, -documents3), rowHeights= 75, manualColumnResize = TRUE,manualRowResize = TRUE,stretchH = "all"
                        #, rowHeaderWidth = 200
          ) %>% hot_col(2:length(select(SDT, -documents3)), colWidths = 180) %>%
            hot_col(col = "category", type = "dropdown", source = categoryField) %>%
            hot_validate_character(cols = "category", choices = c('',categoryField)) %>%
            hot_col(col = "sub-category", type = "dropdown", source = subcategoryField) %>%
            hot_validate_character(cols = "sub-category", choices = c('',NULL,subcategoryField))
        })
      }
    }
    
  })
  
  
  observeEvent(input$fileUploadData,{
    # fileNameGen <- stringGen()
    fileU <- input$fileUploadData
    fileU$datapath
    fileCode<- paste(Sys.Date(),'_',fileU$name,sep='')
    put_object(
      file = fileU$datapath
      # , object = paste(fileNameGen[1],'.pdf',sep='')
      , object = paste('Timeline PDF/',fileCode,sep='')
      , bucket = i360Bucket
      , key = awsKey
      , base_url = endpoint
      , secret = awsSecretAccessKey
    )
    #add timestamp
    docCode <- fileU$name
    print(fileCode)
    showModal(modalDialog(
      title = "SUCCESS",
      paste(
        "File Uploaded. Please enter code"),
      div(print(fileCode), style="font-size:160%"),
      paste('into documents3 field')
    )
    )
  })
  
  
}

# Run the application
shinyApp(ui = ui, server = server)
